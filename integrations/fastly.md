# EINBLIQ.IO CMCD API integration for Fastly

This documentation describes the EINBLIQ.IO CMCD integration for Fastly.

To forward CMCD information from Fastly to the EINBLIQ.IO CMCD API endpoint, the following two integration steps need to be performed:

1. Providing your `ServiceId` to EINBLIQ.IO for log authentication
2. Creating an EINBLIQ.IO Logging Endpoint


### 1. Providing your `ServiceId` to EINBLIQ.IO

In order to forward logs from Fastly to a service such as EINBLIQ.IO, Fastly requires to [authenticate all log requests](https://docs.fastly.com/en/guides/log-streaming-https#prerequisites).

To do so, EINBLIQ.IO needs the `serviceId` of your delivery service, which is forwarding logs to the EINBLIQ.IO CMCD API endpoint.

To find the `serviceId`:

1. Log in to the [Fastly management portal](https://manage.fastly.com/home) 
2. Navigate to the `Deliver` section
3. Copy the `serviceId` from the Service you want to forward logs
4. Provide the `serviceId` to `team@einbliq.io`

![image info](./../images/fastly_1.png)

### 2. Creating an EINBLIQ.IO Logging Endpoint

To create a new endpoint to forward CMCD information to EINBLIQ.IO's CMCD API:

1. Navigate to the `Deliver` Section
2. Select the Service you want to use to forward logs

![image info](./../images/fastly_2.png)

Now, create a new configuration version by:

1. Clicking on `Edit configuration`
2. Selecting `Clone version to edit`

![image info](./../images/fastly_3.png)

On the configuration page, select a new `HTTPS` logging endpoint by:

1. Navigating to the `Logging` section
2. Searching for the `HTTPS` logging endpoint
3. Clicking on `+ Create endpoint`

![image info](./../images/fastly_4.png)

To configure the `HTTPS` endpoint:

1. Provide a Name

![image info](./../images/fastly_5.png)

Further:

1. Specify the Log format. To capture all data necessary, copy the following logging format template and paste it into the Log format section:

```json
{
    "timestamp_utc": "%{strftime(\{"%Y-%m-%dT%H:%M:%S%z"\}, time.start)}V",
    "client_ip": "%{req.http.Fastly-Client-IP}V",
    "host": "%{if(req.http.Fastly-Orig-Host, req.http.Fastly-Orig-Host, req.http.Host)}V",
    "cmcd_request": "%{json.escape(req.http.CMCD-Request)}V",
    "cmcd_object": "%{json.escape(req.http.CMCD-Object)}V",
    "cmcd_status": "%{json.escape(req.http.CMCD-Status)}V",
    "cmcd_session": "%{json.escape(req.http.CMCD-Session)}V",
    "request_query_parameter": "%{json.escape(req.url.qs)}V",
    "url": "%{json.escape(req.url)}V",
    "request_referer": "%{json.escape(req.http.referer)}V",
    "request_user_agent": "%{json.escape(req.http.User-Agent)}V",
    "fastly_server": "%{json.escape(server.identity)}V",
    "fastly_is_edge": %{if(fastly.ff.visits_this_service == 0, "true", "false")}V
  }
```

2. Enter the EINBLIQ.IO CMCD API endpoint URL, which will be provided by `team@einbliq.io`


![image info](./../images/fastly_6.png)

Within the `Advanced options` section:

1. Set the Content type to: `application/json`
2. Add `Authorization` as Custom header name
3. Add the Bearer token value provided by `team@einbliq.io`
4. Select `Post` as HTTP method
4. Select `Array of Json` as JSON log entry format

![image info](./../images/fastly_7.png)

To create the `HTTPS` endoint:

1. Click `Create` on the bottom of the page

![image info](./../images/fastly_8.png)

As a last step, the new configuration needs to be activated. To do so:

1. Click on the `Activate` button on the configuration overview page and wait, until the new configuration is applied.

![image info](./../images/fastly_9.png)

