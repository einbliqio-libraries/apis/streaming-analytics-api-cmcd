# EINBLIQ.IO CMCD API integration for Akamai

This documentation describes the EINBLIQ.IO CMCD integration for Akamai.

To forward CMCD information from Akamai to the EINBLIQ.IO CMCD API endpoint, the following two integration steps need to be performed:

1. Creating an Akamai `DataStream`
2. Enabling and activating `DataStream` in the property configuration


### 1. Creating an Akamai DataStream

The Akamai `DataStream` service can be used, to forward captured CMCD information to the the EINBLIQ.IO CMCD API endpoint.

To create a new `DataStream`:

1. [Log in to the Akamai control center](https://control.akamai.com/)
2. Open the service menue on the left hand side > navigate to the `Common Services` section > and select `DataStream`

![image info](./../images/akamai_1.png)

3. On the `DataStream` overview page, select `create stream` in the top right corner.

4. Now start to configure the new `DataStream` on the configuration page:
 
    1. Provide a name for the new `DataStream`
    2. Select a group, to which the `DataStream` shoud be applied to
    3. Select the properties, to which the `DataStream` should be applied to
    4. Click `Next`


![image info](./../images/akamai_2.png)

5. On the `DataStream` data set page, select the values, which will be forwarded to the EINBLIQ.IO CMCD API endpoint.
    1. Select the following values:
        - `CMCD`
        - `CP code`
        - `Request time`
        - `Client IP`
        - `HTTP status code`
        - `Request host`
        - `Request path`
        - `User-Agent`
        - `Query string`
        - `Error code`
        - `Transfer time`
        - `Time to first byte`
        - `Throughput`
        - `Edge IP`
    2. In the Log file section, select `Json` as log format
    3. click `Next`

![image info](./../images/akamai_3.png)

6. On the `DataStream` delivery page, configure the CMCD API endpoint:
    
    1. Select `Custom HTTPS` as Destination
    2. Create a Display Name for the endpoint
    3. Select `Basic` as Authentication method
    4. Add the enpoint URL (which will be provided by `team@einbliq.io`)
    5. Set `akamai` as user name
    6. Provide the basic authentication password (which will be provided by `team@einbliq.io`)
    7. Set `30` seconds as push frequency
    8. Click `Validate & Save`
    9. Click `Next`

![image info](./../images/akamai_4.png)

7. On the `DataStream` summary page, review the settings and:
    1. Click `Save stream`

![image info](./../images/akamai_5.png)

Now, the Akamai `DataStream` is configured and ready to forward CMCD information.


### 2. Enable and activate `DataStream` in the property configuration

As a seconds step, the newly created `DataStream` must be enabled in the Property Manager Configuration section.

1. To do so, navigate in the Akamai Control Center to Services > CDN > Properties

![image info](./../images/akamai_6.png)

2. Select the Property you want to use, to forward CMCD information

![image info](./../images/akamai_7.png)

3. Select `New Version` in the Active Staging Version section of the Property Details Page

![image info](./../images/akamai_8.png)

4. In the Property Configuration Settings:

    1. Select the rule, to which the `DataStream` service should be applied to
    2. Click `+ Behaviour`
    3. Select `Standard property behaviour`

![image info](./../images/akamai_9.png)

5. In the Behaviour selection:

    1. Locate the `DataStream` behaviour
    2. Click `Insert Behaviour`

![image info](./../images/akamai_10.png)

6. Now, the `DataStream` behaviour is inserted into the property configuration. To apply the newly crated `DataStream`:

    1. Select `DataStream 2` as Stream version
    2. Set Enable to `On`
    3. Select the name of the `DataStream` you created as Stream name
    4. Set the sampling rate to `100`
    5. Click `Save`

![image info](./../images/akamai_11.png)

7. As the last step, the updated property configuration needs to be activated. To do so:

    1. Navigate to the `Activate` tab
    2. Click `Activate on staging`
    3. After testing the new configuration on the staging environment thoroughly, you can activate it to the production environment

![image info](./../images/akamai_12.png)





