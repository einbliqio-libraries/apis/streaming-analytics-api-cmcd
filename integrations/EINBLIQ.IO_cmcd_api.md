# EINBLIQ.IO CMCD API

This documentation describes the EINBLIQ.IO CMCD API.

## Access

The base Url scheme for the API is:

    https://{customerId}-sa-api.1bliq.io


> NOTE: The `customerId` will be provided by team@einbliq.io
	
The `Swagger` documentation is available here:

    https://docs-sa-api.1bliq.io/doc

## Authentication/Authorization

Any client interacting with the APIs needs to provide an access token that has the privilege to access the resource which it is requesting.

> NOTE: The access token will be provided by team@einbliq.io

The API access token needs to be passed as an `Authorization` header in each HTTP request.

*Exampe of an HTTP authenticated request:*
```javascript
GET  https://{customerId}-sa-api.1bliq.io/{resource} HTTP/1.1
Authorization: Bearer YOUR-API-TOKEN
Accept: application/json
```

All APIs are based on the REST and JSON standard.

## HTTP Errors

The EINBLIQ.IO APIs return the following HTTP status codes, if the request is not valid:

|  Error  | Description |
|--|--|
| 400 | A malformed request.|
| 401|  Missing token or Authentication failed. |
| 404 | Resource not found. |
| 429 | Too many requests. |
| 500 | Server Error. |


Errors are returned in the following JSON format:

```javascript
{
	"code":  400,
	"message":  "bad request"
}
```

## Common Media Client Data Endpoint

### POST - /v1/cmcd 

The CMCD endpoint takes an array of `CMCD` objects, as well as the `ip`, from which the data is originating and the `cdn` as the name of the used CDN.  

Each `CMCD` object should apply to the CMCD format specified in the [CTA Specification 5004](https://cdn.cta.tech/cta/media/media/resources/standards/pdfs/cta-5004-final.pdf).

**Example Body**

```json
[
  {
    "ip": "string", //the client source IP
    "cdn": "string", //the CDN name
    "data": { 
      "br": 0,
      "bl": 0,
      "bs": true,
      "cid": "string",
      "d": 0,
      "dl": 0,
      "mtp": 0,
      "nor": "string",
      "nrr": "string",
      "ot": "string",
      "pr": 0,
      "rtp": 0,
      "sf": "string",
      "sid": "string",
      "st": "string",
      "su": true,
      "tb": 0,
      "v": 0
    }
  },
  ...
  ...
]
```

In case of a succesful request, the client receives an HTTP status code `200 OK`.

### Validation

The API performs a formal validation on the incoming payload. In case any validation errors are detected, the API rejects the entire payload and returns a HTTP status code `400` or `422`.

The validation is performed on the following properties of each `CMCD` object:

- `ip` not null or empty
- `data` not null 
