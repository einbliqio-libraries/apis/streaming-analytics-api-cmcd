# AWS S3 bucket integration for edgio

This documentation describes the AWS S3 bucket integration for `edgio`.

To forward CMCD information from `edgio` to an AWS S3 bucket, the AWS S3 Real Time Log Delivery mechanism needs to be activated.


### Activating AWS S3 Real Time Log Delivery

`edgio` is capable of forwarding CDN logs to an AWS S3 bucket. From there, the logs will be picked up by the EINBLIQ.IO service for further processing.

A detailed `edgio` documentation can be found [here](https://docs.edg.io/guides/v7/logs/rtld/aws_s3_log_delivery). 

To forward logs from `edgio` to an AWS S3 bucket:

1. Create a new `Log Delivery Profile` by: 

    - Opening the desired property
    - From the left pane, click on the desired environment
    - From the left pane, click `Realtime Log Delivery`
    - Click `+ New Log Delivery Profile` and then select: `CDN`

2. From the `Profile Name` option, assign a name to this log delivery profile
3. From the `Log Delivery Method` option, select: `AWS S3`
4. Define how Real Time Log Delivery will communicate with AWS S3:
    - Set the `Bucket` option to the name of the AWS S3 bucket to which log data will be posted. Note: the name of the bucket will be provided by `team@einbliq.io`
    - Set the `Prefix` option to: `edgio/`
    - From the `AWS Region` option, select the region assigned to the AWS S3 bucket. Note: the name of the AWS region will be provided by `team@einbliq.io`
5. From the `Log Format` option, select the default `JSON format`
6. Verify that the `Downsample` Logs option is cleared, to forward all CDN logs
7. Click `Create Log Delivery Profile`
