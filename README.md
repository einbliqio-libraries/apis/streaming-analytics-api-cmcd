# EINBLIQ.IO CMCD API

This documentation describes how Common Media Client Data (CMCD) can be captured by different Content Delivery Networks (CDNs) and forwarded to the [EINBLIQ.IO](https://einbliq.io/) service.

After forwarding the CMCD sets, EINBLIQ.IO processes and analyses the captured CMCD information to:

- Measure the regional streaming performances for each ISP-CDN combination
- Feed a quality aware Mult-CDN system (such as EINBLIQ.IO's Content Steering Server)

![image info](./images/cmcd_dataflow.png)

## CDN Integrations

To process all CMCD sets from a CDN, the CDN needs to forward all CMCD sets to EINBLIQ.IO. This can be done either by directly forwarding CMCD sets to the EINBLIQ.IO API endpoint, or through a cloud storage module such as AWS S3, Azure Blob Storage or Google Cloud Storage, from which EINBLIQ.IO picks up the logs autonomously.

As each CDN vendor has its own mechanism to forward CMCD sets to a remote endpoint, the following documentation pages describe the CMCD forwarding meachnism for all CDNs, which are currently supported.

- [Akamai (AWS S3 bucket)](./integrations/akamai_aws_s3 bucket.md)
- [Akamai (custom endpoint)](./integrations/akamai_custom_endpoint.md)
- [edgio (AWS S3 bucket)](./integrations/edgio_aws_s3 bucket.md)
- [fastly (custom endpoint)](./integrations/fastly.md)
- [EINBLIQ.IO CMCD API (for custom CDN integrations)](./integrations/EINBLIQ.IO_cmcd_api.md)

> NOTE: If you are using a CDN which is not yet covered by this documentation, reach out to `team@einbliq.io`.

Latest version: `v1.4.0`